function pairs(obj)
{
    let keys=[]
    let values=[]
    let pair=[]
    keys=Object.keys(obj)
    values=Object.values(obj)
    for(let i=0; i<keys.length;i++)
    {
        let list=[]
        list.push(keys[i])
        list.push(values[i])
        pair.push(list)
    }
    return pair;
}
module.exports=pairs
